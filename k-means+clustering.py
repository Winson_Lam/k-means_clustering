
# coding: utf-8

# In[174]:


import numpy as np
from matplotlib import pyplot as plt
get_ipython().magic('matplotlib inline')


# In[175]:


m = 10
X1 = np.random.multivariate_normal([1, 1], [[1, 0.1], 
                                            [0.1, 1.0]], m)
X2 = np.random.multivariate_normal([5, 5], [[1, 0.1], 
                                            [0.1, 0.1]], m)
X = np.vstack([X1, X2])


# In[176]:


plt.figure(1, figsize=(2, 2))
plt.scatter(X[:, 0], X[:, 1])
plt.title("unlabeled data")


# In[177]:


# Randomly Initialize Centroids
num_centroid = 2


# In[178]:


muDict = {}
for i in range(num_centroid):
    muDict['muK{}'.format(i)] = X[np.random.randint(m)]


# In[179]:


muDict


# In[194]:


muK1 = muDict['muK0']
muK2 = muDict['muK1']


# In[195]:


muK1


# In[196]:


muK2


# In[199]:


np.vstack([J_muK1, J_muK2])


# In[201]:


J_muK1 = np.sqrt(((X - muK1)**2).sum(axis = 1))
J_muK2 = np.sqrt(((X - muK2)**2).sum(axis = 1))
y = np.vstack([J_muK1, J_muK2])
c_i = y.argmin(axis = 0)

muK1 = X[c_i ==0].mean(axis = 0)
muK2 = X[c_i ==1].mean(axis = 0)

plt.figure(1, figsize=(5, 5))

X = np.vstack([X1, X2])
plt.scatter(X[:, 0], X[:, 1], c = c_i)
plt.scatter(muK1[0], muK1[1], s = 10000, alpha= 0.1)
plt.scatter(muK2[0], muK2[1], s = 10000, alpha= 0.1)
plt.title("unlabeled data")


# In[67]:


np.unique(c_i, return_counts=True)

